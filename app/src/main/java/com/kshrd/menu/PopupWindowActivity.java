package com.kshrd.menu;

import android.content.Context;
import android.graphics.Rect;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;

public class PopupWindowActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_popup_window);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.popup_winow_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.insert:
                showPopupWindow();
                break;
            case R.id.share:
                break;
            case R.id.fb:
                break;
            case R.id.twitter:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showPopupWindow() {
        Rect frame = new Rect();
        getWindow().getDecorView().getWindowVisibleDisplayFrame(frame);

        // Margin from Right 20DP
        int xOffset = dp2Px(this, 0F);

        int yOffset = frame.top + getSupportActionBar().getHeight();
        //xOffset += 20; // Will add more marngin from ActionBar

        final View parentView = getLayoutInflater().inflate(R.layout.activity_popup_window, null);
        View customView = getLayoutInflater().inflate(R.layout.custom_popup_window, null);

        final PopupWindow popupWindow = new PopupWindow(
                customView,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT,
                true
        );

        popupWindow.setAnimationStyle(android.R.style.Animation_Dialog);

        popupWindow.showAtLocation(
                parentView,
                Gravity.TOP | Gravity.RIGHT,
                xOffset,
                yOffset
        );

        customView.findViewById(R.id.btnClose).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupWindow.dismiss();
            }
        });
    }

    private int dp2Px(Context context, float dp) {
        float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dp * scale + 0.5f);
    }
}
