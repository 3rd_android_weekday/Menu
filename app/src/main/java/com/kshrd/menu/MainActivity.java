package com.kshrd.menu;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.PopupMenu;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnContextMenu = (Button) findViewById(R.id.btnGenderContextMenu);
        Button btnContextMenuCountry = (Button) findViewById(R.id.btnContextMenuCountry);
        Button btnSubjectMenu = (Button) findViewById(R.id.btnPopUpMenu);

        registerForContextMenu(btnContextMenu);
        registerForContextMenu(btnContextMenuCountry);

        btnSubjectMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu = new PopupMenu(MainActivity.this, v);
                popupMenu.inflate(R.menu.subject_menu);
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.php:
                                Toast.makeText(MainActivity.this, "PHP", Toast.LENGTH_SHORT).show();
                                break;
                            case R.id.vb:
                                Toast.makeText(MainActivity.this, "VB", Toast.LENGTH_SHORT).show();
                                break;
                            case R.id.java:
                                Toast.makeText(MainActivity.this, "Java", Toast.LENGTH_SHORT).show();
                                break;
                        }
                        return true;
                    }

                });
                popupMenu.setOnDismissListener(new PopupMenu.OnDismissListener() {
                    @Override
                    public void onDismiss(PopupMenu menu) {
                        Toast.makeText(MainActivity.this, "on Dismissed", Toast.LENGTH_SHORT).show();
                    }
                });
                popupMenu.show();
            }
        });

        btnSubjectMenu.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Toast.makeText(MainActivity.this, "Long", Toast.LENGTH_SHORT).show();
                return true;
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.option_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.insert:
                Toast.makeText(this, "Add", Toast.LENGTH_SHORT).show();
                break;
            case R.id.delete:
                Toast.makeText(this, "Delete", Toast.LENGTH_SHORT).show();
                break;
            case R.id.share:
                Toast.makeText(this, "Share", Toast.LENGTH_SHORT).show();
                break;
            case R.id.upload:
                Toast.makeText(this, "Upload", Toast.LENGTH_SHORT).show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        if (v.getId() == R.id.btnGenderContextMenu) {
            getMenuInflater().inflate(R.menu.gender_menu, menu);
            menu.setHeaderTitle("Gender");
        } else if (v.getId() == R.id.btnContextMenuCountry) {
            getMenuInflater().inflate(R.menu.country_menu, menu);
            menu.setHeaderTitle("Country");
        }

    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.male:
                Toast.makeText(this, "Male", Toast.LENGTH_SHORT).show();
                break;
            case R.id.female:
                Toast.makeText(this, "Female", Toast.LENGTH_SHORT).show();
                break;
            case R.id.other:
                Toast.makeText(this, "Other", Toast.LENGTH_SHORT).show();
                break;
            case R.id.kh:
                Toast.makeText(this, "Cambodia", Toast.LENGTH_SHORT).show();
                break;
            case R.id.th:
                Toast.makeText(this, "Thailand", Toast.LENGTH_SHORT).show();
                break;
            case R.id.vn:
                Toast.makeText(this, "Vietnam", Toast.LENGTH_SHORT).show();
                break;
        }
        //Toast.makeText(this, item.getTitle(), Toast.LENGTH_SHORT).show();
        return super.onContextItemSelected(item);
    }
}
